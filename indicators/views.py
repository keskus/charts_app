from django.shortcuts import render
from django.http import HttpResponse
import random
import django
import datetime

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.gridspec as gridspec
from matplotlib import cm
import numpy as np
import numpy.random  as random
import pandas as pd
random.seed(1234)

import io

data  = [{ }]

def home(request): 
	return render(request, 'indicators/home.html')

def heatmap(request): 
	rc = {
		'lines.linewidth': 1.0,
		'axes.facecolor': '0.995',
		'figure.facecolor': '0.97',
		'font.family': ['sans-serif'],
		'font.sans-serif': ['Arial',
		 'DejaVu Sans',
		 'Liberation Sans',
		 'Bitstream Vera Sans',
		 'sans-serif'],
		'font.monospace': 'Ubuntu Mono',
		'font.size': 10,
		'axes.labelsize': 14,
		'axes.labelweight': 'bold',
		'axes.labelpad': 14,
		'axes.titlesize': 14,
		'xtick.labelsize': 12,
		'ytick.labelsize': 12,
		'legend.fontsize': 10,
		'figure.titlesize': 12
	}
	sns.set_style("ticks", {"xtick.major.pad": 8, "ytick.major.pad": 8})
	
	sns.set_context(rc)
	sns.set_style("dark")
	sns.set_palette("deep", desat=.6)


	data = random.uniform(-1, 1, size = (10,10) )
	def create_name(num): 
		return 'indicator' +  str(num)
	indicator_names = list(map(create_name, np.arange(0,9) ))
	indicator_names.append('Anomaly Score')
	data_df = pd.DataFrame(data, index = ['tickerA', 'tickerB', 'tickerC', 'tickerD', 'tickerE','tickerF'
		,'tickerG','tickerH','tickerI','tickerJ'], 
		columns = indicator_names)
	
	fig=plt.figure(figsize = (14,14))
	gs = gridspec.GridSpec(1, 3, wspace=0.25, hspace=0.5)
	ax=fig.add_subplot(gs[:,:])
	ax.xaxis.tick_top()
	ax.xaxis.set_label_position('top') 
	
	heatmap = sns.heatmap(
		data_df,
		annot=True,
		fmt="0.1f",
		annot_kws={"size": 8},
		alpha=1.0,
		center=0.0,
		cbar=False,
		cmap = cm.seismic,
		ax=ax)
	
	ax.set_yticklabels(ax.get_yticklabels(), rotation=0)

	ax.set_ylabel('Tickers',  fontweight='bold')
	ax.set_xlabel('Indicators',  fontweight='bold')
	
	datetime_str = datetime.datetime(2018, 12,12,17,53,30).strftime("%H:%M %Y-%m-%d")

	plt.figtext(0.80,0.10, 'Last updated: {}'.format(datetime_str))
	#ax=fig.add_subplot(gs[:,-1])

	#high_values = data_df.nlargest(10, data_df.columns)
	#plt.table(high_values, ax = ax)
	
	buf = io.BytesIO()
	plt.savefig(buf, format = 'png')
	plt.close(fig)
	#canvas=FigureCanvas(fig)
	response=django.http.HttpResponse(buf.getvalue(), content_type = 'image/png')
	
	#response=django.http.HttpResponse(content_type='image/png')
	#canvas.print_png(response)
	return response